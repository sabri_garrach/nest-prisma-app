import { PrismaClient } from '@prisma/client';
import { publications } from './publications.seed';

const prisma = new PrismaClient();

async function main() {
  for (let publication of publications) {
    await prisma.publication.create({
      data: publication,
    });
  }
}

main()
  .catch((e) => {
    console.error(e);
    process.exit(1);
  })
  .finally(async () => {
    await prisma.$disconnect();
  });