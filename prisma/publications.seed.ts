export const publications = [
  {
    name: "SUP Board 12'",
    description: "Inflatable SUP Board 12' in RedOrange.",
    price: 549.99,
    sku: 'sku_supboard_12_red',
    published: true,
  },
  {
    name: 'Flex Touring Fin Blue',
    price: 26.9,
    sku: 'sku_fin_flex_touring_blue',
    published: true,
  },
  {
    name: 'Touring Fin Red',
    price: 20.9,
    sku: 'sku_fin_touring_red',
    published: false,
  },
];