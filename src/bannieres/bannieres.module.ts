import { Module } from '@nestjs/common';
import { BannieresService } from './bannieres.service';
import { BannieresController } from './bannieres.controller';

@Module({
  controllers: [BannieresController],
  providers: [BannieresService]
})
export class BannieresModule {}
