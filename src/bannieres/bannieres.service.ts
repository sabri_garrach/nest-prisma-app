import { Injectable } from '@nestjs/common';
import { CreateBanniereDto } from './dto/create-banniere.dto';
import { UpdateBanniereDto } from './dto/update-banniere.dto';

@Injectable()
export class BannieresService {
  create(createBanniereDto: CreateBanniereDto) {
    return 'This action adds a new banniere';
  }

  findAll() {
    return `This action returns all bannieres`;
  }

  findOne(id: number) {
    return `This action returns a #${id} banniere`;
  }

  update(id: number, updateBanniereDto: UpdateBanniereDto) {
    return `This action updates a #${id} banniere`;
  }

  remove(id: number) {
    return `This action removes a #${id} banniere`;
  }
}
