import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { BannieresService } from './bannieres.service';
import { CreateBanniereDto } from './dto/create-banniere.dto';
import { UpdateBanniereDto } from './dto/update-banniere.dto';

@Controller('bannieres')
export class BannieresController {
  constructor(private readonly bannieresService: BannieresService) {}

  @Post()
  create(@Body() createBanniereDto: CreateBanniereDto) {
    return this.bannieresService.create(createBanniereDto);
  }

  @Get()
  findAll() {
    return this.bannieresService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.bannieresService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateBanniereDto: UpdateBanniereDto) {
    return this.bannieresService.update(+id, updateBanniereDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.bannieresService.remove(+id);
  }
}
