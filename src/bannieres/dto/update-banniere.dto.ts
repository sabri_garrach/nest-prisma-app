import { PartialType } from '@nestjs/swagger';
import { CreateBanniereDto } from './create-banniere.dto';

export class UpdateBanniereDto extends PartialType(CreateBanniereDto) {}
