import { Injectable } from '@nestjs/common';
import { CreatePublicationDto } from './dto/create-publication.dto';
import { UpdatePublicationDto } from './dto/update-publication.dto';
import { PrismaService } from '../prisma/prisma.service';
@Injectable()
export class PublicationsService {

constructor(private prisma: PrismaService) {}



  create(createPublicationDto: CreatePublicationDto) {
    return this.prisma.publication.create({ data: createPublicationDto });
  }

  findAll() {
    return this.prisma.publication.findMany({ where: { published: true } });
  }

  findOne(id: number) {
    return this.prisma.publication.findUnique({ where: { id: id } });
  }

  update(id: number, updatePublicationDto: UpdatePublicationDto) {
     return this.prisma.publication.update({
		where: { id: id },
		data: updatePublicationDto,
	});
  }

  remove(id: number) {
    return this.prisma.publication.delete({ where: { id: id } });;
  }
}
