import { Controller, Get, Post, Body, Patch, Param, Delete,Put } from '@nestjs/common';
import { PublicationsService } from './publications.service';
import { CreatePublicationDto } from './dto/create-publication.dto';
import { UpdatePublicationDto } from './dto/update-publication.dto';
import { ApiTags,ApiResponse,ApiCreatedResponse , ApiOkResponse } from '@nestjs/swagger';
import { PublicationEntity } from "./entities/publication.entity";

@ApiTags('publications')
@Controller('publications')
export class PublicationsController {
  constructor(private readonly publicationsService: PublicationsService) {}

  @Post()
@ApiCreatedResponse({ type: PublicationEntity })
 async create(@Body() createPublicationDto: CreatePublicationDto) {
    return new PublicationEntity(await this.publicationsService.create(createPublicationDto));
    
  }

  @Get()
  @ApiOkResponse({ type: [PublicationEntity] })
  async findAll() {
    const publications = await this.publicationsService.findAll();
    return publications.map((pub) => new PublicationEntity(pub));
  }

  @Get(':id')
  @ApiOkResponse({ type: PublicationEntity })
  async findOne(@Param('id') id: string) {
    return new  PublicationEntity( await this.publicationsService.findOne(+id));
  }

  @Patch(':id')
  @ApiCreatedResponse({ type: PublicationEntity })
 async update(@Param('id') id: string, @Body() updatePublicationDto: UpdatePublicationDto) {
    return new  PublicationEntity(await this.publicationsService.update(+id, updatePublicationDto));
  }
@Put(':id')
  @ApiCreatedResponse({ type: PublicationEntity })
 async updatewithPut(@Param('id') id: string, @Body() updatePublicationDto: UpdatePublicationDto) {
    return new  PublicationEntity(await this.publicationsService.update(+id, updatePublicationDto));
  }
  @Delete(':id')
  @ApiOkResponse({ type: PublicationEntity })
 async remove(@Param('id') id: string) {
    return new  PublicationEntity(await this.publicationsService.remove(+id));
  }
}
