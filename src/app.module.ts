import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PrismaService } from './prisma/prisma.service';
import { UsersModule } from './users/users.module';
import { BannieresModule } from './bannieres/bannieres.module';
import { DossiersModule } from './dossiers/dossiers.module';
import { ArticlesModule } from './articles/articles.module';
import { AuthModule } from './auth/auth.module';
import { PublicationsModule } from './publications/publications.module';
import { PrismaModule } from './prisma/prisma.module';
import { UsersModule } from './users/users.module';
@Module({
  imports: [AuthModule, UsersModule, PublicationsModule, PrismaModule, ArticlesModule, DossiersModule, BannieresModule],
  controllers: [AppController],
  providers: [AppService,PrismaService],
})
export class AppModule {}
